package ShapeAnalysis

import (
	"fmt"
	"log"
	"math"

	"github.com/montanaflynn/stats"
	Polyfit "gitlab.cern.ch/bnl-omega-go/polyfit"
)

type ShapeAnalysisResults struct {
	Trise, Tbl, T_peak, Amplitude, Pedestal float64
	Event                                   int64
	Metadata                                string
}

func (s ShapeAnalysisResults) String() string {
	return fmt.Sprintf("Waveform %v (%v) has rise of %0.2f ns, peaking time of %0.2f ns  fall time of %0.2f ns, Amplitude of %0.3f mV and Pedestal of %0.3f mV",
		s.Event, s.Metadata, s.Trise/1e-9, (s.T_peak-s.Trise)/1e-9, s.Tbl/1e-9, s.Amplitude/1e-3, s.Pedestal/1e-3)
}

func FindMaximum(times, values []float64) (float64, float64) {
	// Get rough maximum
	max := -1.0
	max_i := 0
	for i, value := range values {
		if math.Abs(value) > max {
			max = math.Abs(value)
			max_i = i
		}
	}

	// Fit peak point with order 2 poly
	//fmt.Println(values, times, max, max_i)
	t_fitpoints := times[max_i-3 : max_i+1]
	fitpoints := values[max_i-3 : max_i+1]
	result := []float64{times[max_i], max, 0.05}

	for i := 0; i < 10; i++ {
		//fmt.Println(result)
		result = Polyfit.Gaussfit(t_fitpoints, fitpoints, 2, result)
	}
	//result := polyfit.Gaussfit(t_fitpoints, fitpoints, 2, []float64{0, 0, 0})

	//Return time corresponding to amplitude (peaking time), + fitted value of amplitude
	//return x_max, (result[3]*(x_max-result[0])*(x_max-result[0]) + result[2]*(x_max-result[0]) + result[1])

	//return result[0], result[1]
	return times[max_i], max
}

func FindPedestal(value []float64, n int64) (pedestal float64) {
	var err error
	pedestal, err = stats.Mean(value[0:n])

	if err != nil {
		log.Fatal(err)
	}
	return
}

func FindRiseTime(times, values []float64, amplitude float64) float64 {

	t0, p0, t1, p1 := 0., 0., 0., 0.

	for i, value := range values[0 : len(values)-1] {
		if value >= 0.05*amplitude {
			break
		}

		t0, p0, t1, p1 = times[i], values[i], times[i+1], values[i+1]
	}

	result := Polyfit.Polyfit([]float64{t0, t1}, []float64{p0 / amplitude, p1 / amplitude}, 1, []float64{p1, (p1 - p0) / (t1 - t0)})
	t_5p := (0.05 - result[0]) / result[1]

	//fmt.Println([]float64{t0, t1}, []float64{p0 / amplitude, p1 / amplitude}, result, t_5p)

	t2, p2, t3, p3 := 0., 0., 0., 0.

	for i, value := range values[0 : len(values)-1] {
		if value >= 0.95*amplitude {
			break
		}
		t2, p2, t3, p3 = times[i], values[i], times[i+1], values[i+1]
	}

	result = Polyfit.Polyfit([]float64{t2, t3}, []float64{p2 / amplitude, p3 / amplitude}, 1, []float64{p3, (p3 - p2) / (t3 - t2)})
	t_95p := (0.95 - result[0]) / result[1]

	//fmt.Println([]float64{t2, t3}, []float64{p2 / amplitude, p3 / amplitude}, result, t_95p)

	return t_95p - t_5p
}

func delta(a, b float64) float64 {
	return math.Abs(a - b)
}

func FindReturnToBaseline(times, values []float64, t_peak, baseline float64) float64 {
	t0, p0, t1, p1 := 0., 0., 0., 0.
	n := len(values)
	for i, _ := range values {
		j := n - 1 - i
		if delta(baseline, values[j])/baseline > 0.05 {
			break
		}
		t0, p0, t1, p1 = times[j], values[j], times[j-1], values[j-1]
	}

	result := Polyfit.Polyfit([]float64{t0, t1}, []float64{p0 / baseline, p1 / baseline}, 1, []float64{1, 1})

	t_5p := (0.95 - result[0]) / result[1]

	return t_5p - t_peak

}

func DoShapeAnalysis(times, values []float64, event int64, metadata string) ShapeAnalysisResults {

	result := ShapeAnalysisResults{0., 0., 0., 0., 0., 0, ""}
	result.T_peak, result.Amplitude = FindMaximum(times, values)
	result.Pedestal = FindPedestal(values, 10)
	result.Trise = FindRiseTime(times, values, result.Amplitude)
	// result.Tbl = FindReturnToBaseline(times, values, result.T_peak, result.Pedestal)
	result.Event = event
	result.Metadata = metadata

	return result
}
